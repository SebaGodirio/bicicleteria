var Bicicleta = function(id, color, modelo) {
    this.id = id
    this.color = color
    this.modelo = modelo
}

Bicicleta.prototype.toString = function() {
    return `id: ${this.id} color: ${this.color} modelo: ${this.modelo}`
}

Bicicleta.allBicis = []
Bicicleta.add = function(aBici) {
    Bicicleta.allBicis.push(aBici)
}

Bicicleta.findById = function(aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId)
    if (aBici)
        return aBici
    else
        throw new Error('No existe una bicicleta con el id ' + aBiciId)
}

Bicicleta.removeById = function(aBiciId) {

    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i, 1)
            break
        }
    }
}

var a = new Bicicleta(1, 'rojo', 'urbana')
var b = new Bicicleta(2, 'verde', 'rural')
var c = new Bicicleta(3, 'azul', 'rural')

Bicicleta.add(a)
Bicicleta.add(b)
Bicicleta.add(c)

module.exports = Bicicleta