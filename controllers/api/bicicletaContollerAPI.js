var Bicicleta = require('../../models/bicicleta')

exports.bicicleta_list = function(req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    })
}

exports.bicicleta_create = function(req, res) {
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo)

    Bicicleta.add(bici)

    res.status(201).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = function(req, res) {
    var id = req.body.id
    Bicicleta.removeById(id)
    res.status(203).json({
        bicicleta: id
    })
}

exports.bicicleta_update = function(req, res) {
    var id = req.body.id
    var bici = Bicicleta.findById(id)
    bici.id = req.body.id
    bici.color = req.body.color
    bici.modelo = req.body.modelo

    res.status(202).json({
        bicicleta: bici
    })
}