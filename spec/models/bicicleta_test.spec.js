var Bicicleta = require('../../models/bicicleta')

beforeEach(() => { Bicicleta.allBicis = [] })

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
    })
})

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0)

        var a = new Bicicleta(1, 'rojo', 'urbana')
        Bicicleta.add(a)

        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0]).toEqual(a)
    })
})

describe('Bicicleta.findById', () => {
    it('buscamos una', () => {
        var a = new Bicicleta(4, 'rojo', 'urbana')
        Bicicleta.add(a)
        expect(Bicicleta.findById(4)).toEqual(a)
    })
})